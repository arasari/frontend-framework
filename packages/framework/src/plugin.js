import * as components from './components';
import * as filters from './filters';
import * as plugins from './plugins';
import mixins from './mixins';
import axios from 'axios';
import VeeValidate from 'vee-validate';
import VueMeta from 'vue-meta';
import PortalVue from 'portal-vue';

export default {
    install(Vue) {
        Vue.use(VeeValidate, {errorBagName: '__errors', fieldsBagName: '__fields',});
        Vue.use(VueMeta);
        Vue.use(PortalVue);

        Object.entries(components).forEach(([name, value]) => Vue.component(name, value));
        Object.entries(filters).forEach(([name, value]) => Vue.filter(name, value));
        Object.values(plugins).forEach(value => Vue.use(value));
        mixins.forEach(mixin => Vue.mixin(mixin));

        axios.defaults.baseURL = '/api/';
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
    }
};
