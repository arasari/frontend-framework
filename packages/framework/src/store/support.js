export function bootModules(store, modules) {
    const inits = [];
    Object
        .keys(modules)
        .forEach(name => {
            const module = modules[name];
            if (module.actions.init) {
                inits.push(store.dispatch(`${name}/init`));
            }
        });
    return Promise.all(inits);
}
