export default {
    link: {
        mark_all_as_read: 'Отметить как прочитанные',
        goto_home: 'На главную',
    },
    menu: {
        dashboard: 'Dashboard',
        search: 'Поиск',
    },
    label: {
        drop_or_click_to_upload: 'Перетащите или выберите файл',
        drop_files: 'Перетяните сюда файл',
        remove_this_file: 'Удалить',
    },
    page_title: {
        app_name: 'Asarari App',
        settings: 'Настройки',
    },
    title: {
        profile_photo: 'Аватар',
        change_password: 'Сменить пароль',
        upload_photos: 'Загрузить фото',
        dashboard: 'Dashboard',
        error_404: 'Страница не найдена',
    },
    text: {
        no_items_yet: 'Нет данных.'
    },
    toast: {
        http_error: 'Произошла ошибка, поторите попытку позже.',
    },
    button: {
        save: 'Сохранить',
        reset: 'Сбросить',
        select_file: 'Выбрать файл',
        remove_profile_image: 'Удалить',
        search: 'Поиск',
    },
    validator: {
        required: 'Это поле обязательно.',
        minLength: 'Нужно ввести не менее {min} символов.',
        sameAs: 'Значения не одинаковы.',
        currentPassword: 'Неверный текущий пароль.',
    }
};
