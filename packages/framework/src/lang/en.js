export default {
    link: {
        mark_all_as_read: 'Mark all as read',
        goto_home: 'Back to homepage',
    },
    menu: {
        dashboard: 'Dashboard',
        search: 'Search',
    },
    label: {
        email: 'Email',
        name: 'Name',
        full_name: 'Name',
        avatar: 'Photo',
        current_password: 'Current password',
        new_password: 'New password',
        password_confirmation: 'Confirm password',
        description: 'Description',
        phone: 'Phone',
        password: 'Password',
        drop_or_click_to_upload: 'Drop or select a file',
        drop_files: 'Drop files here',
        remove_this_file: 'Remove',
    },
    page_title: {
        app_name: 'Asarari App',
        settings: 'Settings',
    },
    placeholder: {
        enter_email: 'Enter E-mail address',
        enter_password: 'Enter new password',
        confirm_password: 'Enter new password',
    },
    title: {
        profile_photo: 'Profile photo',
        change_password: 'Change password',
        upload_photos: 'Upload photos',
        dashboard: 'Dashboard',
        error_404: 'Page not found',
    },
    text: {
        no_items_yet: 'No data.'
    },
    toast: {
        http_error: 'An error has occurred. Please, try again later.',
    },
    button: {
        reset: 'Reset',
        save: 'Save',
        select_file: 'Select a file',
        remove_profile_image: 'Remove',
        search: 'Search',
    },
    validator: {
        required: 'This field is required.',
        minLength: 'The length must be at least {min}.',
        sameAs: 'Values did not match.',
        currentPassword: 'Current password is invalid.',
    }
};
