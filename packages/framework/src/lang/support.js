import Vue from 'vue';
import moment from 'moment';
import flatpickr from 'flatpickr';
import {Validator} from 'vee-validate';

export function localizeLibs(locale) {
    moment.locale(locale);
    flatpickr.l10ns.default.firstDayOfWeek = 1;

    import(`vee-validate/dist/locale/${locale}.js`).then(msg => {
        Validator.localize(locale, msg.default);
    });

    import(`flatpickr/dist/l10n/${locale}.js`).then(msg => {
        flatpickr.localize(msg.default[locale]);
    });
}

export function setupLocalization(i18n, locale) {
    Vue.filter('trans', function(...args) {
        return i18n.t(...args);
    });

    Vue.mixin({
        methods: {
            __(...args) {
                return i18n.t(...args);
            }
        }
    });

    if (locale != 'en') {
        localizeLibs(locale);
    }
}
