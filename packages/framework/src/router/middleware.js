import NProgress from 'nprogress';
import 'nprogress/nprogress.css';

export const nprogress = ({router}) => {
    let progressTimer = -1;
    router.beforeEach((to, from, next) => {
        if (progressTimer) {
            clearTimeout(progressTimer);
            NProgress.done();
        }

        progressTimer = setTimeout(() => {
            NProgress.start();
        }, 200);

        next();
    });
    router.afterEach(() => {
        clearTimeout(progressTimer);
        NProgress.done();
    });

    router.onError(error => {
        NProgress.done();
        console.error(error);
    });
};

export const guards = ({router, store}) => {
    /**
     * "auth" guard. allow only authenticated users
     */
    router.beforeEach((to, from, next) => {
        if (to.matched.some(record => record.meta.auth)) {
            if (!store.getters['auth/authenticated']) {
                console.log('redirect');
                next({
                    name: 'login',
                    query: {
                        redirect: to.fullPath
                    }
                });
            } else {
                next();
            }
        } else {
            next();
        }
    });

    /**
     * "dispatch": dispatches a store action before navigation.
     */
    router.beforeEach(async(to, from, next) => {
        const route = to.matched.find(record => record.meta.dispatch);
        if (route) {
            await store.dispatch(route.meta.dispatch);
            next();
        } else {
            next();
        }
    });
};

export default [nprogress, guards];
