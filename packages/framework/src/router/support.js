/**
 * Apply a list of middlewares on router instance.
 *
 * @param router
 * @param store
 * @param middleware
 */
export function applyMiddleware(router, store, middleware) {
    middleware.forEach(mw => mw({router, store}));
}

/**
 * The default implementation of scroll behavior.
 *
 * @param to
 * @param from
 * @param savedPosition
 * @returns {*}
 */
export function scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
        return savedPosition;
    } else {
        return {x: 0, y: 0};
    }
}
