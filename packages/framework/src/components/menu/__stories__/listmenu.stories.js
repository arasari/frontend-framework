import Vue from 'vue';
import {storiesOf} from '@storybook/vue';

import * as components from '../index';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Menu/ListMenu', module)
    .add('default', () => ({
        template: `
        <list-menu>
            <list-menu-item>Menu item 1</list-menu-item>
            <list-menu-item>Menu item 2</list-menu-item>
            <list-menu-item>Menu item 3</list-menu-item>
            <list-menu-item>Menu item 4</list-menu-item>
            <list-menu-item>Menu item 5</list-menu-item>
        </list-menu>`
    }))
    .add('with icon', () => ({
        template: `
        <list-menu>
            <list-menu-item icon="fe fe-user">Menu item 1</list-menu-item>
            <list-menu-item icon="fe fe-home">Menu item 2</list-menu-item>
            <list-menu-item icon="fa fa-gear">Menu item 3</list-menu-item>
            <list-menu-item icon="fa fa-filter">Menu item 4</list-menu-item>
            <list-menu-item icon="fa fa-wrench">Menu item 5</list-menu-item>
        </list-menu>`
    }))
    .add('transparent background', () => ({
        template: `
        <list-menu transparent>
            <list-menu-item>Menu item 1</list-menu-item>
            <list-menu-item>Menu item 2</list-menu-item>
            <list-menu-item>Menu item 3</list-menu-item>
            <list-menu-item>Menu item 4</list-menu-item>
            <list-menu-item>Menu item 5</list-menu-item>
        </list-menu>`
    }))
;
