import Vue from 'vue';
import {storiesOf} from '@storybook/vue';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Icons/Icon', module)
    .add('default icon', () => ({
        template: `<icon name="fe fe-share"></icon>`
    }))
;

storiesOf('Icons/CloseIcon', module)
    .add('default icon', () => ({
        description: `
            CloseIcon is tiny utility component which renders an icon which may be used in modals,
            alerts or everywhere else where we have a close functionality.
           
           This component emits **close** icon so you are able to handle it.
        `,
        template: `<div style="padding: 20px"> 
            <close-icon></close-icon>
        </div>`
    }))
;
