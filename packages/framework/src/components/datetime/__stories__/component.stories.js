import Vue from 'vue';
import {storiesOf} from '@storybook/vue';
import {withKnobs, text} from '@storybook/addon-knobs';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

storiesOf('DateTime/TimeAgo', module)
    .addDecorator(withKnobs)
    .add('standard', () => ({
        template: `<time-ago value="${text('Date', '2018-01-01')}"></time-ago>`
    }))
;
