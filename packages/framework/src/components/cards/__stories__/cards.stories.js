import {storiesOf} from '@storybook/vue';
import Vue from 'vue';

Object.entries(require('../../buttons')).forEach(([name, component]) => Vue.component(name, component));
Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Cards/Card', module)
    .add('simple', () => ({
        description: `
            The card container is the only required element in a card. 
            All other elements shown here are optional.
            
            Each card is made up of content blocks. All of the blocks, as a whole, 
            are related to a single subject or destination. 
            Content can receive different levels of emphasis, depending on its level of hierarchy.    
        `,
        template: `<card>
        <card-body>
        I am a very simple card. I am good at containing small bits of information. 
        I am convenient because I require little markup to use effectively.
        </card-body>
    </card>`
    }))

    .add('with header', () => ({
        template: `<card>
        <card-header>
            <card-title>Card title</card-title>
        </card-header>
        <card-body>
        I am a very simple card. I am good at containing small bits of information. 
        I am convenient because I require little markup to use effectively.
        </card-body>
        <card-footer>
            <btn color="primary">Action 1</btn>
            <btn color="light">Action 2</btn>
        </card-footer>
    </card>`
    }))


    .add('with heading (shortcut)', () => ({
        template: `<card>
        <card-heading>Card title</card-heading>
        <card-body>
        I am a very simple card. I am good at containing small bits of information. 
        I am convenient because I require little markup to use effectively.
        </card-body>
        <card-footer>
            <btn color="primary">Action 1</btn>
            <btn color="light">Action 2</btn>
        </card-footer>
    </card>`
    }))
;
