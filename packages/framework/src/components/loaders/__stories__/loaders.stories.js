import Vue from 'vue';
import {storiesOf} from '@storybook/vue';
import {action} from '@storybook/addon-actions';

import * as components from '../index';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Loaders/Spinner', module)
    .add('default', () => ({
        description: `
            Use this component to indicate that there is some action in the background.
        `,
        template: `<div>
            <spinner></spinner>
        </div>`
    }))
;

storiesOf('Loaders/OverlayLoader', module)
    .add('default', () => ({
        description: `
            Use this component when you want to disable some content from user interaction while the content 
            is not ready or not loaded yet.
            
            Important note, that the loader by default has 300ms delay. 
            This is an intended behaviour and made to prevent loader from visual blinking when the promise (or "active" prop)
            resolves too fast.
        `,
        template: `<overlay-loader active>
            <div>This is a content.</div>
        </overlay-loader>`
    }))

    .add('with promise', () => ({
        description: `
            Similarly to Button, OverlayLoader exposes \`activeWhile\` method which receives a promise
            and waits for it resolution. During the waiting the OverlayLoader will be visible. 
        `,
        template: `
        <div>
            <overlay-loader ref="loader">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.  
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                It has survived not only five centuries, but also the leap into electronic typesetting, 
                remaining essentially unchanged. 
                It was popularised in the 1960s with the release of Letraset sheets containing 
                Lorem Ipsum passages, and more recently with desktop publishing software like 
                Aldus PageMaker including versions of Lorem Ipsum.
            </overlay-loader>
            
            <button @click="onClick" type="button">Start loading (3 secs)</button>
        </div>
    `,
        methods: {
            onClick() {
                const promise = new Promise(resolve => {
                    setTimeout(resolve, 3000);
                });
                this.$refs.loader.activeWhile(promise);
            }
        }
    }))
;
