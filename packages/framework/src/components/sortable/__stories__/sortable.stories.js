import {storiesOf} from '@storybook/vue';

import Vue from 'vue';

Object.entries(require('..')).forEach(([name, component]) => Vue.component(name, component));

const items = [
    {id: 1, name: 'Minsk'},
    {id: 2, name: 'Warsaw'},
    {id: 3, name: 'Zurich'},
    {id: 4, name: 'London'},
    {id: 5, name: 'Tokyo'},
];

storiesOf('SortableList', module)
    .add('default', () => ({
        data() {
            return {items};
        },
        template: `
            <div>items: {{items}}
                <sortable-list v-model="items">
                    <div slot-scope="{items}">
                        <sortable-item v-for="item in items" :key="item.id">
                            <div>
                                {{ item.name }}
                                <sortable-handle>
                                    <span>handle</span>                                                    
                                </sortable-handle>
                            </div>                
                        </sortable-item>
                    </div>            
                </sortable-list>
            </div>
        `,
    }))
;
