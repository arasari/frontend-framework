import Vue from 'vue';
import {storiesOf} from '@storybook/vue';
import { action } from '@storybook/addon-actions';

import * as components from '../index';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Alerts/Alert', module)
    .add('color variants', () => ({
        description: `
            \`color\` property receives any value supported by Bootstrao or your theme CSS.
            
            If you need a custom color,
            create a new CSS class with prefix "alert-$name" and use $name as a value for "color" prop.          
        `,
        template: `<div>
            <alert color="info">This is info (default) alert</alert>
            <alert color="success">This is success alert</alert>
            <alert color="danger">This is danger alert</alert>
            <alert color="warning">This is warning alert</alert>
            <alert color="secondary">This is secondary alert</alert>
            <alert color="light">This is light alert</alert>
            <alert color="dark">This is dark alert</alert>
        </div>`
    }))

    .add('with heading', () => ({
        description: `
            Pass \`heading\` property to add a heading to the alert.
        `,
        template: `<alert heading="This is a heading">This is info (default) alert</alert>`
    }))

    .add('with icon', () => ({
        description: `
            Pass \`icon\` property to add an icon.
        `,
        template: `<alert heading="This is a heading" icon="fa fa-wrench">This is info (default) alert</alert>`
    }))

    .add('closeable', () => ({
        description: `
            When alert has \`closeable\` property then it will emit **close** event each time when close icon is clicked.
            It is up to developer how to handle it (show/hide, enable/disable). 
            This is by design because otherwise there would be a mismatch in states of alert and outer components 
            (one this alert is shown, other - is hidden) and a developer would need to care about this each time.  
            \`\`\`js
            export default {
                template: '<alert closeable @close="onClose">This is info (default) alert</alert>'
                methods: {
                    onClose: () => console.log('close clicked')                
                }
            }
            \`\`\`
        `,
        methods: {
            onClose: action('close'),
        },
        template: `<alert closeable @close="onClose">This is info (default) alert</alert>`
    }))
;
