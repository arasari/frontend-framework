import Vue from 'vue';
import {storiesOf} from '@storybook/vue';
import {withKnobs, boolean, text} from '@storybook/addon-knobs';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Collapse', module)
    .addDecorator(withKnobs)
    .add('standard', () => ({
        template: `<collapse>This is wrapped content.</collapse>`
    }))

    .add('with custom labels', () => ({
        template: `<collapse 
            text-open="${text('Open text', 'Custom open text')}" 
            text-close="${text('Close text', 'Custom close text')}" 
            >This is wrapped content.</collapse>`
    }))

    .add('active state', () => ({
        template: `<collapse ${boolean('Active', true) ? 'active': ''}>This is wrapped content.</collapse>`
    }))

;
