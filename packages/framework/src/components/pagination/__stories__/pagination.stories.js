import {storiesOf} from '@storybook/vue';
import {withKnobs, text, number} from '@storybook/addon-knobs';

import Vue from 'vue';

Object.entries(require('..')).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Pagination', module)
    .addDecorator(withKnobs)
    .add('default', () => ({
        description: `
            Bind a \`v-model\` to paginator to watch the changes  or listen to \`@input\` event.
            
            The availabel props are:
            * \`total\` - a total number of items (objects, not pages)
            * \`pageSize\` - page size (how many items show per page)
            * \`visible\` - controls number of visible buttons on paginator tape
            * \`prevLabel\` - the label of the "go to previous" button 
            * \`nextLabel\` - the label of the "go to next" button 
            * \`value\` - current page number (should be controlled vua \`v-model\`) 
        `,
        template: `<div>
            current page: {{ page }}
            <pagination 
            total="${number('Total', 100)}" 
            page-size="${number('Page size', 20)}" 
            :visible="${number('Visible count', 15)}" 
            prev-label="${text('Prev label', 'Prev')}" 
            next-label="${text('Next label', 'Next')}" 
            v-model="page"
            ></pagination>
        </div>`,
        data() {
            return {
                page: 1
            };
        }
    }))
    .add('disabled', () => ({
        template: `<pagination disabled total="100"></pagination>`,
    }))
;
