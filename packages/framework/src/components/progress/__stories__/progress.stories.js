import {storiesOf} from '@storybook/vue';
import {withKnobs, number} from '@storybook/addon-knobs';

import Vue from 'vue';

Object.entries(require('..')).forEach(([name, component]) => Vue.component(name, component));

const options = {
    range: true,
    min: 0,
    max: 100,
    step: 1
};

storiesOf('Progress', module)
    .addDecorator(withKnobs)
    .add('default', () => ({
        template: `<progress-bar progress="${number('Progress', 75, options)}"></progress-bar>`,
    }))
    .add('indeterminate', () => ({
        template: `<progress-bar indeterminate></progress-bar>`,
    }))
;
