import Vue from 'vue';
import {storiesOf} from '@storybook/vue';
import {withKnobs} from '@storybook/addon-knobs';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

const onDestroyed = () => {
    Array
        .from(document.body.querySelectorAll('.modal-backdrop'))
        .forEach(el => el.parentNode.removeChild(el));
};

storiesOf('Dialogs/ConfirmationDialog', module)
    .addDecorator(withKnobs)
    .add('with v-model', () => ({
        data() {
            return {active: false};
        },
        destroyed: onDestroyed,
        template: `
        <div>
            model value: "{{ active }}"
            <btn @click="active = !active">Toggle</btn>
            <modal v-model="active" title="This is a modal header.">
                This is a modal content.
                <template slot="actions">
                    <btn flat>Deny</btn>            
                    <btn color="primary">Accept</btn>            
                </template>
            </modal>
        </div>`
    }))
;
