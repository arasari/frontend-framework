import Vue from 'vue';
import {storiesOf} from '@storybook/vue';
import {withKnobs, text, boolean} from '@storybook/addon-knobs';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

const onDestroyed = () => {
    Array
        .from(document.body.querySelectorAll('.modal-backdrop'))
        .forEach(el => el.parentNode.removeChild(el));
};

storiesOf('Dialogs/Modal', module)
    .addDecorator(withKnobs)
    .add('default', () => ({
        destroyed: onDestroyed,
        template: `<modal active title="${text('Title', 'This is a title')}">
            This is a modal content.
        </modal>`
    }))
    .add('with title', () => ({
        destroyed: onDestroyed,
        template: `<modal active title="${text('Title', 'This is a modal header.')}">
            This is a modal content.
        </modal>`
    }))
    .add('with actions', () => ({
        destroyed: onDestroyed,
        template: `<modal active title="This is a modal header.">
            This is a modal content.
            <template slot="actions">
                <btn flat>Deny</btn>            
                <btn color="primary">Accept</btn>            
            </template>
        </modal>`
    }))
    .add('with no backdrop', () => ({
        destroyed: onDestroyed,
        template: `<modal active no-backdrop title="${text('Title', 'This is a modal header.')}">
            This is a modal content.
        </modal>`
    }))
    .add('disable page scroll', () => ({
        destroyed: onDestroyed,
        template: `<div>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            <modal active locked title="${text('Title', 'This is a modal header.')}">
                This is a modal content.
            </modal>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
</div>`
    }))
    .add('toggleable', () => ({
        destroyed: onDestroyed,
        template: `<modal :active="${boolean('Active', true)}" title="This is a modal header.">
            This is a modal content.
            <template slot="actions">
                <btn flat>Deny</btn>            
                <btn color="primary">Accept</btn>            
            </template>
        </modal>`
    }))
    .add('with v-model', () => ({
        data() {
            return {active: false};
        },
        destroyed: onDestroyed,
        template: `
        <div>
            model value: "{{ active }}"
            <btn @click="active = !active">Toggle</btn>
            <modal v-model="active" title="This is a modal header.">
                This is a modal content.
                <template slot="actions">
                    <btn flat>Deny</btn>            
                    <btn color="primary">Accept</btn>            
                </template>
            </modal>
        </div>`
    }))
    .add('dismiss via slot scope', () => ({
        data() {
            return {active: false};
        },
        destroyed: onDestroyed,
        template: `
        <div>
            model value: "{{ active }}"
            <btn @click="active = !active">Toggle</btn>
            <modal v-model="active" title="This is a modal header.">
                This is a modal content.
                <template slot="actions" slot-scope="{dismiss}">
                    <btn flat @click="dismiss">Deny</btn>            
                    <btn color="primary" @click="dismiss">Accept</btn>            
                </template>
            </modal>
        </div>`
    }))
;
