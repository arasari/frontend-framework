import {storiesOf} from '@storybook/vue';

import Vue from 'vue';

import * as components from '../index';

import IMG from './image.png';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Fields/ImageField', module)
    .add('standard', () => ({
        template: `<image-field name="field" src="${IMG}"></image-field>`,
    }))
;

