import {storiesOf} from '@storybook/vue';
import {withKnobs, number} from '@storybook/addon-knobs';

import Vue from 'vue';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Fields/RangeField', module)
    .addDecorator(withKnobs)
    .add('standard', () => ({
        template: `<range-field 
            from="${number('From', 1980)}" 
            to="${number('To', 2020)}" 
            step="${number('Step', 10)}" 
            name="field"></range-field>`,
    }))
    .add('with placeholder', () => ({
        template: `<range-field 
            placeholder="Select an year"
            from="${number('From', 1980)}" 
            to="${number('To', 2020)}" 
            step="${number('Step', 10)}" 
            name="field"></range-field>`,
    }))
;

