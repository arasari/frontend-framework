import {storiesOf} from '@storybook/vue';
import {withKnobs, text} from '@storybook/addon-knobs';

import Vue from 'vue';

import * as components from '../index';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Fields/PasswordField', module)
    .addDecorator(withKnobs)
    .add('standard', () => ({
        template: `<password-field name="field"></password-field>`,
    }))
    .add('with reveal', () => ({
        data() {
            return {model: text('Password', 'password')};
        },
        template: `<password-field v-model="model" reveal name="field"></password-field>`,
    }))
;

