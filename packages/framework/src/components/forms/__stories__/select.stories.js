import {storiesOf} from '@storybook/vue';

import Vue from 'vue';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

const options = [
    {id: 1, name: 'Minsk'},
    {id: 2, name: 'Berlin'},
    {id: 3, name: 'London'},
    {id: 4, name: 'Tokyo'},
    {id: 5, name: 'Warsaw'},
];

const altOptions = [
    {key: 1, value: 'Minsk'},
    {key: 2, value: 'Berlin'},
    {key: 3, value: 'London'},
    {key: 4, value: 'Tokyo'},
    {key: 5, value: 'Warsaw'},
];

const computed = {
    options: () => options,
    altOptions: () => altOptions,
};

storiesOf('Forms/Fields/SelectField', module)
    .add('standard', () => ({
        computed,
        description: `
            \`SelectField\` expects \`options\` property to be an array of objects. 
            Each objects must have \`id\` (value) and \`name\` (label) attributes.
            \`\`\`js
            const options = [
                {id: 1, name: 'Minsk'},
                {id: 2, name: 'Berlin'},
                {id: 3, name: 'London'},
                {id: 4, name: 'Tokyo'},
                {id: 5, name: 'Warsaw'},
            ];
            \`\`\`
        `,
        template: `<select-field name="field" :options="options"></select-field>`,
    }))
    .add('required', () => ({
        computed,
        template: `<select-field required name="field" :options="options"></select-field>`,
    }))
    .add('multiple', () => ({
        computed,
        data: () => ({model: []}),
        template: `<div>
            selected: {{model}}
            <select-field v-model="model" multiple required name="field" :options="options"></select-field>
        </div>`,
    }))
    .add('with placeholder', () => ({
        computed,
        template: `<select-field placeholder="Select an item" name="field" :options="options"></select-field>`,
    }))
    .add('required with placeholder', () => ({
        computed,
        template: `<select-field required placeholder="Select an item" name="field" :options="options"></select-field>`,
    }))
    .add('with custom label and key provider', () => ({
        computed,
        description: `
            If your data objects do not have id-name, 
            then you can specify alternate attribute keys via \`label-key\` and \`value-key\` props.: 
            \`\`\`js
            const options = [
                {key: 1, value: 'Minsk'},
                {key: 2, value: 'Berlin'},
                {key: 3, value: 'London'},
                {key: 4, value: 'Tokyo'},
                {key: 5, value: 'Warsaw'},
            ];
            \`\`\`
        `,
        template: `<select-field name="field" label-key="value" value-key="key" :options="altOptions"></select-field>`,
    }))
;

