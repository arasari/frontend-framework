import {storiesOf} from '@storybook/vue';

import Vue from 'vue';

import * as components from '../index';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Fields/FileField', module)
    .add('standard', () => ({
        template: `<file-field name="field"></file-field>`,
    }))
    .add('custom contents', () => ({
        template: `<file-field name="field">Click me now!</file-field>`,
    }))
    .add('multiple', () => ({
        template: `<file-field multiple name="field"></file-field>`,
    }))
    .add('no preview', () => ({
        template: `<file-field :preview="false" name="field"></file-field>`,
    }))
;

