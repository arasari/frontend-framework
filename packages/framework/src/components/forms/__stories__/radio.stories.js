import {storiesOf} from '@storybook/vue';

import Vue from 'vue';

import * as components from '../index';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Fields/RadioField', module)
    .add('standard', () => ({
        template: `<radio-field name="radio" value="label">Label</radio-field>`,
    }))
    .add('group', () => ({
        data() {
            return {
                model: 'b'
            };
        },
        template: `<div>
            model: {{model}}
            <radio-field v-model="model" name="radio" value="a">A</radio-field>
            <radio-field v-model="model" name="radio" value="b">B</radio-field>
            <radio-field v-model="model" name="radio" value="c">C</radio-field>
        </div>`,
    }))
;

