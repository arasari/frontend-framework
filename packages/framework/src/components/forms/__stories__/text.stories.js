import {storiesOf} from '@storybook/vue';

import Vue from 'vue';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Fields/TextField', module)
    .add('standard', () => ({
        template: `<text-field name="field"></text-field>`,
    }))
    .add('multiline', () => ({
        template: `<text-field multiline name="field"></text-field>`,
    }))
;

