import {storiesOf} from '@storybook/vue';
import {text, boolean, withKnobs} from '@storybook/addon-knobs';

import Vue from 'vue';

import * as components from '../index';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Droparea', module)
    .add('default', () => ({
        description: `
            An area which accepts file drops
        `,
        template: `<droparea @input="onDrop"></droparea>`,
        methods: {
            onDrop(files) {
                alert(`Dropped ${files.length} files`);
            }
        }
    }))

    .add('custom template', () => ({
        template: `<droparea @input="onDrop">Drop <b>files</b> here...</droparea>`,
        methods: {
            onDrop(files) {
                alert(`Dropped ${files.length} files`);
            }
        }
    }))
;

storiesOf('Forms/FormGroup', module)
    .addDecorator(withKnobs)
    .add('default', () => ({
        description: `
            FormGroup is a component which wraps form fields and takes care of labels, error and validation.
            
            If field is required then group will render required asterist next to the label.
        `,
        template: `<form-group 
                        label="${text('Label', 'Field label')}"
                        help="${text('Help text', 'Field help message')}"
                    >
                <text-field 
                name="text" ${boolean('Required', false) ? 'required' : ''} 
                value="Text..."></text-field>
            </form-group>`,
    }))

    .add('with custom label', () => ({
        description: `
            You may want to render more complex label than default. Then implement \`label\` scoped slot.
            THe original label content will be available via \`label\` scoped slot property.
            
        `,
        template: `<form-group 
                        label="${text('Label', 'Field label')}"
                        help="${text('Help text', 'Field help message')}"
                    >
                <text-field 
                name="text" ${boolean('Required', false) ? 'required' : ''} 
                value="Text..."></text-field>
                
                <template slot="label" slot-scope="{label}">
                    Custom label with <button>button</button>.
                    (original label: {{label}})
                </template>
            </form-group>`,
    }))

    .add('with error message', () => ({
        description: `
            By default, FormGroup controls error messages automatically.
            But in case if you want to control errors manually, you can override \`errors\` scoped slot.  
            The messages, collected from the field, will be available via \`errors\` slot prop.
        `,
        template: `<form-group label="${text('Label', 'Password')}">
                <text-field 
                name="text" ${boolean('Required', false) ? 'required' : ''} 
                value="Text..."></text-field>
                
                <template slot="errors" slot-scope="{errors}">
                    <div class="text-danger">I am always visible!</div>
                </template>
            </form-group>`,
    }))
;
