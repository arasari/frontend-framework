import {text, withKnobs} from '@storybook/addon-knobs';
import {boolean} from '@storybook/addon-knobs/src/index';
import {storiesOf} from '@storybook/vue';

import Vue from 'vue';

import * as components from '../index';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Form', module)
    .addDecorator(withKnobs)
    .add('common props', () => ({
        description: `
            The \`v-form\` emits **submit** event each time when the form was submitted 
            and it is valid (the component automatically validates its fields).
            
            Each form field must bind \`v-model\` to make validation and form properly forms.  
            Another requirement is that every field also has to have \`name\` field.
            
            The standard form structure is like this:
            \`\`\`html
            <!-- add a form element and add a submit handler -->
            <v-form @submit="onSubmit">
                <!-- form fields go here -->
                <form-group label="Field label">
                    <text-field name="text" v-model="text"></text-field>                
                </form-group>   
                
                <!-- add form-actions and put submit and other button into it -->
                <form-actions>
                    <!-- the button must have "submit" type -->
                    <btn type="submit" color="primary">Save</btn>
                </form-actions>
            </v-form>
            \`\`\``,
        data() {
            return {
                formData: {
                    email: '',
                    password: '',
                    remember: false,
                }
            };
        },
        template: `<v-form legend="${text('Legend', 'Login')}" help="${text('Help', 'Log into your accout to access all features')}">
            <form-group label="E-Mail Address">
                <email-field name="email" v-model="formData.email" />
            </form-group>
            <form-group label="Password">
                <password-field name="password" v-model="formData.password" />
            </form-group>
            <form-group>
                <checkbox-field name="remember" label="Remember me" v-model="formData.remember" />
            </form-group>
            <form-actions>
                <btn type="submit" color="primary">Login</btn>
            </form-actions>
        </v-form>`,
    }))

    .add('simple', () => ({
        data() {
            return {
                formData: {
                    email: '',
                    password: '',
                    remember: false,
                }
            };
        },
        template: `<v-form>
            <form-group label="E-Mail Address">
                <email-field name="email" v-model="formData.email" />
            </form-group>
            <form-group label="Password">
                <password-field name="password" v-model="formData.password" />
            </form-group>
            <form-group>
                <checkbox-field name="remember" label="Remember me" v-model="formData.remember" />
            </form-group>
            <form-actions>
                <btn type="submit" color="primary">Login</btn>
            </form-actions>
        </v-form>`,
    }))

    .add('with validation', () => ({
        description: `
            Each form input has \`rules\` property defining validation rules for the form field.
            
            Note, that is you use standard HTML's \`required\` attribute 
            will also automatically enable \`required\` validator rule.
            Validation rules are fired on \`blur\` event.
        `,
        data() {
            return {
                formData: {
                    email: '',
                    password: '',
                    remember: false,
                }
            };
        },
        template: `<v-form>
            <form-group label="E-Mail Address">
                <email-field name="email" required v-model="formData.email" />
            </form-group>
            <form-group label="Password">
                <password-field name="password" required rules="required|min:3" v-model="formData.password" />
            </form-group>
            <form-group>
                <checkbox-field name="remember" label="Remember me" v-model="formData.remember" />
            </form-group>
            <form-actions>
                <btn type="submit" color="primary">Login</btn>
            </form-actions>
        </v-form>`,
    }))

    .add('usage with v-model', () => ({
        description: `
            Form can report validity state via v-model
        `,
        data() {
            return {
                isValid: true,
                formData: {
                    email: '',
                    password: '',
                    remember: false,
                }
            };
        },
        template: `<v-form v-model="isValid">
            is valid: {{ isValid }}
            <form-group label="E-Mail Address">
                <email-field name="email" required v-model="formData.email" />
            </form-group>
            <form-group label="Password">
                <password-field name="password" required rules="required|min:3" v-model="formData.password" />
            </form-group>
            <form-group>
                <checkbox-field name="remember" label="Remember me" v-model="formData.remember" />
            </form-group>
            <form-actions>
                <btn type="submit" color="primary" :disabled="!isValid">Login</btn>
            </form-actions>
        </v-form>`,
    }))

    .add('set errors', () => ({
        description: `
            You can set form errors by calling \`setErrors\` method.  
            Supported formats are:
            \`\`\`js
             {
                 field_name: ['errors', 'array'],
                 non_field_errors: ['same', 'format'],
                 detail: 'string message'
             }
             \`\`\`
             
             \`field_name\` is a name of form field and messages will be rendered directly under this field.   
              \`non_field_errors\` and \`detail\` are plain text and shown above the form in the alert component.
        `,
        methods: {
            setErrors() {
                this.$refs.form.setErrors({
                    email: ['Email is invalid.', 'Email is taken.'],
                    detail: 'An error message.',
                });
            }
        },
        template: `<v-form ref="form">
            <form-group label="E-Mail Address">
                <email-field name="email" required />
            </form-group>
            <form-group label="Password">
                <password-field name="password" required rules="required|min:3" />
            </form-group>
            <form-group>
                <checkbox-field name="remember" label="Remember me" />
            </form-group>
            <form-actions>
                <btn type="submit" color="primary" :disabled="!isValid">Login</btn>
                <btn @click="setErrors" color="danger">Set errors</btn>
            </form-actions>
        </v-form>`,
    }))
;
