import {storiesOf} from '@storybook/vue';

import Vue from 'vue';

import * as components from '../index';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Fields/EmailField', module)
    .add('standard', () => ({
        template: `<email-field name="field">Label</email-field>`,
    }))
;

