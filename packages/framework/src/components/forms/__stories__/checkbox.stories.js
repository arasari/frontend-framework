import {storiesOf} from '@storybook/vue';
import {text, boolean, withKnobs} from '@storybook/addon-knobs';

import Vue from 'vue';

import * as components from '../index';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Fields/CheckboxField', module)
    .add('standard', () => ({
        template: `<checkbox-field name="checkbox">Label</checkbox-field>`,
    }))
    .add('group', () => ({
        data() {
            return {
                model: ['b']
            };
        },
        template: `<div>
            model: {{model}}
            <checkbox-field v-model="model" name="checkbox" value="a">A</checkbox-field>
            <checkbox-field v-model="model" name="checkbox" value="b">B</checkbox-field>
            <checkbox-field v-model="model" name="checkbox" value="c">C</checkbox-field>
        </div>`,
    }))
;

