import {storiesOf} from '@storybook/vue';

import Vue from 'vue';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Fields/StaticField', module)
    .add('standard', () => ({
        description: `This is a readonly field.`,
        template: `<static-field name="field" value="Some value"></static-field>`,
    }))
    .add('with form group', () => ({
        template: `<form-group label="First name"><static-field name="field" value="Alex"></static-field></form-group>`,
    }))
;

