import {storiesOf} from '@storybook/vue';

import Vue from 'vue';

import * as components from '../index';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Fields/PhoneField', module)
    .add('standard', () => ({
        data: () => ({model: ''}),
        template: `<phone-field v-model="model" name="field"></phone-field>`,
    }))
    .add('national format', () => ({
        template: `<phone-field format="National" name="field"></phone-field>`,
    }))
;

