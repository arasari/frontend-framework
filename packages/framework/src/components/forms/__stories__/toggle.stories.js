import {storiesOf} from '@storybook/vue';

import Vue from 'vue';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Forms/Fields/ToggleField', module)
    .add('standard', () => ({
        template: `<toggle-field name="field">Accept terms?</toggle-field>`,
    }))
    .add('disabled', () => ({
        template: `<toggle-field disabled multiline name="field">Accept terms?</toggle-field>`,
    }))
;

