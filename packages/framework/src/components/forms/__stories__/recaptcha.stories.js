import {storiesOf} from '@storybook/vue';
import {withKnobs, text} from '@storybook/addon-knobs';

import Vue from 'vue';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

const SITEKEY = '6LdY_nkUAAAAAEqkS87856iIbWGhbqrHSzxYfIsK\n';

storiesOf('Forms/Fields/RecaptchaField', module)
    .addDecorator(withKnobs)
    .add('minimal', () => ({
        template: `<recaptcha-field
            sitekey="${text('Site key', SITEKEY)}" 
            name="field"></recaptcha-field>`,
    }))
    .add('dark theme', () => ({
        template: `<recaptcha-field
            theme="dark"
            sitekey="${text('Site key', SITEKEY)}" 
            name="field"></recaptcha-field>`,
    }))
    .add('compact', () => ({
        template: `<recaptcha-field
            size="compact"
            sitekey="${text('Site key', SITEKEY)}" 
            name="field"></recaptcha-field>`,
    }))
    .add('language', () => ({
        template: `<recaptcha-field
            lang="be"
            sitekey="${text('Site key', SITEKEY)}" 
            name="field"></recaptcha-field>`,
    }))
;

