import {storiesOf} from '@storybook/vue';

import Vue from 'vue';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

const options = [
    {id: 1, name: 'Minsk'},
    {id: 2, name: 'Berlin'},
    {id: 3, name: 'London'},
    {id: 4, name: 'Tokyo'},
    {id: 5, name: 'Warsaw'},
];

const altOptions = [
    {key: 1, value: 'Minsk'},
    {key: 2, value: 'Berlin'},
    {key: 3, value: 'London'},
    {key: 4, value: 'Tokyo'},
    {key: 5, value: 'Warsaw'},
];

const computed = {
    options: () => options,
    altOptions: () => altOptions,
};

storiesOf('Forms/Fields/SelectizeField', module)
    .add('single choice', () => ({
        computed,
        description: `
            \`SelectizeField\` behaves in same ways as \`SelectField\`.
        `,
        template: `<selectize-field name="field" :options="options"></selectize-field>`,
    }))
    .add('allow own values', () => ({
        computed,
        description: `Allows the user to create new items that aren't in the initial list of options.`,
        template: `<selectize-field create name="field" :options="options"></selectize-field>`,
    }))
    .add('multiple', () => ({
        computed,
        data: () => ({model: []}),
        template: `<div>
            selected: {{model}}
            <selectize-field v-model="model" multiple required name="field" :options="options"></selectize-field>
        </div>`,
    }))
    .add('with placeholder', () => ({
        computed,
        template: `<selectize-field placeholder="Select an item" name="field" :options="options"></selectize-field>`,
    }))
    .add('with custom label and key provider', () => ({
        computed,
        description: `
            If your data objects do not have id-name, 
            then you can specify alternate attribute keys via \`label-key\` and \`value-key\` props.: 
            \`\`\`js
            const options = [
                {key: 1, value: 'Minsk'},
                {key: 2, value: 'Berlin'},
                {key: 3, value: 'London'},
                {key: 4, value: 'Tokyo'},
                {key: 5, value: 'Warsaw'},
            ];
            \`\`\`
        `,
        template: `<selectize-field name="field" label-key="value" value-key="key" :options="altOptions"></selectize-field>`,
    }))
;

