const FieldMixin = {
    props: {
        name: {type: String, required: true},
        value: {required: false},
        rules: {type: String, default: ''},
    },
    inject: {
        formGroup: {
            default: null
        }
    },
    created() {
        if (this.formGroup) {
            this.formGroup.field = this;
        }
    },
    computed: {
        required() {
            return this.$attrs && this.$attrs.hasOwnProperty('required');
        },
    },
    methods: {
        getValue() {
            return this.value;
        },
        onInput(e) {
            if (e instanceof Event) {
                this.$emit('input', e.target.value);
            } else {
                this.$emit('input', e);
            }
        },
        onChange(e) {
            this.$emit('change', e.target.value);
        },
        onBlur(e) {
            this.$emit('blur', e);
        },
        onFocus(e) {
            this.$emit('focus', e);
        }
    }
};

export default FieldMixin;

export function createInputField(name, type) {
    return {
        name: name,
        mixins: [FieldMixin],
        render(h) {
            return h('input', {
                attrs: {
                    name: this.name,
                    type: type,
                    ...this.$attrs,
                },
                on: {
                    input: this.onInput.bind(this),
                    change: this.onChange.bind(this),
                    focus: this.onFocus.bind(this),
                    blur: this.onBlur.bind(this),
                },
                class: 'form-control',
                value: this.value,
            });
        }
    };
}
