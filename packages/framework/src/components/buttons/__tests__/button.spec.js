import {createLocalVue, mount, RouterLinkStub} from '@vue/test-utils';

import Btn from '../Btn';

describe('Button.vue', () => {
    test('should change button type via "type" prop', () => {
        const vm = mount(Btn, {
            propsData: {type: 'submit'}
        });
        expect(vm.element.getAttribute('type')).toEqual('submit');
    });

    test('should change button color via "color" prop', () => {
        const vm = mount(Btn, {
            propsData: {color: 'primary'}
        });
        expect(vm.is('.btn-primary')).toBeTruthy();
    });

    test('should change button size via "size" prop', () => {
        const vm = mount(Btn, {
            propsData: {size: 'lg'}
        });
        expect(vm.is('.btn-lg')).toBeTruthy();
    });

    test('should render disabled button if disabled prop is true', () => {
        const vm = mount(Btn, {
            propsData: {disabled: true}
        });
        expect(vm.is('[disabled]')).toBeTruthy();
    });

    test('should change button style to block via "block" prop', () => {
        const vm = mount(Btn, {
            propsData: {block: true}
        });
        expect(vm.is('.btn-block')).toBeTruthy();
    });

    test('should change button style to outline via "outline" prop', () => {
        const vm = mount(Btn, {
            propsData: {outline: true, color: 'primary'}
        });
        expect(vm.is('.btn-outline-primary')).toBeTruthy();
    });

    test('should add icon to button', () => {
        const Icon = require('../../icons/Icon').default;
        const localVue = createLocalVue();
        localVue.component('icon', Icon);

        const vm = mount(Btn, {
            localVue,
            propsData: {icon: 'fa fa-users'}
        });
        expect(vm.contains('.fa-users')).toBeTruthy();
    });

    test('should add right icon to button', () => {
        const Icon = require('../../icons/Icon').default;
        const localVue = createLocalVue();
        localVue.component('icon', Icon);

        const vm = mount(Btn, {
            localVue,
            propsData: {iconRight: 'fa fa-users'}
        });
        expect(vm.contains('.fa-users')).toBeTruthy();
    });

    test('should render link if href is passed', () => {
        const vm = mount(Btn, {
            propsData: {href: 'https://example.com', target: '_blank'}
        });
        expect(vm.is('a')).toBeTruthy();
        expect(vm.attributes().href).toEqual('https://example.com');
        expect(vm.attributes().target).toEqual('_blank');
    });

    test('should render router-link if route is passed', () => {
        const vm = mount(Btn, {
            propsData: {route: 'homepage'},
            stubs: {
                RouterLink: RouterLinkStub
            }
        });
        expect(vm.find(RouterLinkStub).props().to).toEqual({name: 'homepage'});

        const vm2 = mount(Btn, {
            propsData: {route: {name: 'homepage'}},
            stubs: {
                RouterLink: RouterLinkStub
            }
        });
        expect(vm2.find(RouterLinkStub).props().to).toEqual({name: 'homepage'});
    });

    test('should be disabled while promise is not resolved', done => {
        const w = mount(Btn);
        w.vm.disableWhile(new Promise(resolve => {}));
        expect(w.is('[disabled]')).toBeTruthy();
        done();
    });

});
