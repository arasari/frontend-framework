import Vue from 'vue';
import {storiesOf} from '@storybook/vue';
import * as components from '../index';

Object.entries(components).forEach(([name, component]) => Vue.component(name, component));
Object.entries(require('../../icons')).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Buttons/Btn', module)
    .add(
        'color variants',
        () => ({
            template: `<div>
                <btn>Default</btn>
                <btn color="primary">Primary</btn>
                <btn color="secondary">Secondary</btn>
                <btn color="info">Info</btn>
                <btn color="success">Success</btn>
                <btn color="warning">Warning</btn>
                <btn color="danger">Danger</btn>
                <btn color="light">Light</btn>
                <btn color="dark">Dark</btn>
            </div>`
        }),
        {
            description: `
                Set \`color\` property to change color. The value of this property is a standard Bootstrap's name:
                "danger", "primary", "success", etc.   
                
                You may have own colors defined in CSS: just define \`btn-$colorname\`
                class and use \`$colorname\` as value for "color" prop.`
        })

    .add('size variants', () => ({
        description: `
            Set \`size\` property to change the size. The value of this property is a standard Bootstrap's name:
            "lg", "sm", etc. 
            
            You may have own colors defined in CSS: just define \`btn-$colorname\`
            class and use \`$colorname\` as value for "color" prop.
        `,
        template: `<div>
            <btn size="lg">Large</btn>
            <btn>Default</btn>
            <btn size="sm">Small</btn>
        </div>`
    }))

    .add('outline (ghost) button', () => ({
        description: `
            Ghost buttons are generally bordered by a very thin line, while the internal section consists of plain text. 
            They most often appear as Call to Action (CTA) buttons and offer a clean look.
            
            To make a button a "ghost" button add \`outline\` attribute to a regular button. 
            You may also use "color" and "size" properties for extra control of the view.
        `,
        template: `<div>
            <btn outline color="primary">Primary</btn>        
            <btn outline color="danger">Danger</btn>        
            <btn outline color="warning">Warning</btn>        
            <btn outline color="success">Success</btn>        
            <btn outline color="info">Informational</btn>        
        </div>`
    }))

    .add('block button', () => ({
        template: `<div>
            <btn block color="primary">Primary</btn>        
            <btn block color="danger">Danger</btn>        
            <btn block color="warning">Warning</btn>        
            <btn block color="success">Success</btn>        
            <btn block color="info">Informational</btn>        
        </div>`
    }))

    .add('with icons', () => ({
        description: `
            Button accepts \`icon\` property and renders an icon at the left size of the button.
            
            The format of property value is very common: "$iconset $name".   
            For example: "md md-account-box" for Material Design icon set or, 
            if you prefer Font Awesome, "fa fa-users".  
        `,
        template: `<div>
            <btn icon="fe fe-user">Users</btn>        
            <btn icon="fe fe-trash" color="danger">Delete</btn>        
            <btn outline icon-right="fe fe-edit" color="primary">Edit</btn>        
        </div>`
    }))

    .add('link buttons', () => ({
        description: `
            When you need to have a button to look like a link but behave as a button,
            pass \`link\` boolean attribute. It will make the button to mimic links.
            
            Note, this only styles button as a link. If you need a button to behave as a link
            then use "btn btn-link" classes on link tag.
        `,
        template: `<div>
            <btn color="link">Users</btn>
            <btn color="link">OK</btn>
        </div>`
    }))

    .add('disabled state', () => ({
        description: `
            Disable buttons disallow user interaction.
        `,
        template: `<div>
            <p>Standard</p>
            <btn disabled>Default</btn>
            <btn color="primary" disabled>Primary</btn>
            <btn color="info" disabled>Info</btn>
            <btn color="success" disabled>Success</btn>
            <btn color="warning" disabled>Warning</btn>
            <btn color="danger" disabled>Danger</btn>
            <hr/>
            <p>Outline</p>
            <btn outline disabled>Default</btn>        
            <btn outline color="primary" disabled>Primary</btn>        
            <btn outline color="danger" disabled>Danger</btn>        
            <btn outline color="warning" disabled>Warning</btn>        
            <btn outline color="success" disabled>Success</btn>        
            <btn outline color="info" disabled>Informational</btn>      
            <hr/>
            <p>Flat</p>
            <btn flat disabled icon="md md-account-box">Users</btn>
            <btn flat disabled icon="md md-edit" color="primary">Edit</btn>
            <btn flat disabled color="success">OK</btn>
            <btn flat disabled color="warning">Cancel</btn>
            <p>With icon</p>
            <btn disabled icon="md md-account-box">Users</btn>
            <btn icon="md md-edit" color="primary" disabled>Edit</btn>        
        </div>`
    }))

    .add('disabled state while awaiting promise', () => ({
        description: `
           This component has helper method \`disableWhile\` which accepts an unresolved promise
           as the argument. The button will be in disabled state until the promise resolves/rejects.
             
           Mostly used by forms.
        `,
        template: `<btn ref="btn" color="primary" @click="save">Save</btn>`,
        methods: {
            save() {
                this.$refs.btn.disableWhile(new Promise(resolve => setTimeout(resolve, 2000)));
            }
        }
    }))
;
