import Vue from 'vue';
import {storiesOf} from '@storybook/vue';
import { action } from '@storybook/addon-actions';

Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

storiesOf('Buttons/BtnIcon', module)
    .add('color variants', () => ({
        template: `<div>
            <btn-icon icon="fa fa-users">Default</btn-icon>
        </div>`
    }))

    .add('disabled state', () => ({
        description: `
            Disable buttons disallow user interaction.
        `,
        template: `<btn-icon disabled icon="fa fa-users" @click="click">Default</btn-icon>`,
        methods: {
            click: action('click')
        }
    }))
;
