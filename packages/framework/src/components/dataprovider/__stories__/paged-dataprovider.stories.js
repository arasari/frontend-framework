import Vue from 'vue';
import {storiesOf} from '@storybook/vue';


Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

const items = [
    {id: 1, name: 'Minsk'},
    {id: 2, name: 'Warsaw'},
    {id: 3, name: 'Prague'},
    {id: 4, name: 'Tokyo'},
    {id: 5, name: 'Zurich'},
];

function provider() {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve({results: items, total: 200});
        }, 2000);
    });
}

function providerWithError() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject('Ooops, a loading error has occured...');
        }, 2000);
    });
}

storiesOf('DataProvider/PagedDataProvider', module)
    .add('default', () => ({
        template: `<div>
                <paged-data-provider autoload :provider="provider" ref="loader">
                    <div slot-scope="{items}">
                        items: {{ items }}
                    </div>
                </paged-data-provider>
                <button @click="load">Reload</button>
        </div>`,
        computed: {
            provider: () => provider
        },
        methods: {
            load() {
                this.$refs.loader.load();
            }
        }
    }))

    .add('with error', () => ({
        template: `<div>
                <paged-data-provider autoload :provider="provider" ref="loader">
                    <div slot-scope="{items}">
                        items: {{ items }}
                    </div>
                </paged-data-provider>
                <button @click="load">Reload</button>
        </div>`,
        computed: {
            provider: () => providerWithError
        },
        methods: {
            load() {
                this.$refs.loader.load();
            }
        }
    }))
;
