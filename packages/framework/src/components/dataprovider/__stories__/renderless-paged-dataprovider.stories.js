import Vue from 'vue';
import {storiesOf} from '@storybook/vue';


Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));
Object.entries(require('../../pagination')).forEach(([name, component]) => Vue.component(name, component));

const items = [
    {id: 1, name: 'Minsk'},
    {id: 2, name: 'Warsaw'},
    {id: 3, name: 'Prague'},
    {id: 4, name: 'Tokyo'},
    {id: 5, name: 'Zurich'},
];

function provider() {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve({results: items, total: items.length,})
        }, 2000);
    });
}

storiesOf('DataProvider/RenderlessPagedDataProvider', module)
    .add('default', () => ({
        template: `<div>
                <renderless-paged-data-provider autoload :provider="provider" ref="loader">
                    <div slot-scope="props">
                        <p>isLoading: {{ props.isLoading }}</p>
                        <p>isFirstLoad: {{ props.isFirstLoad }}</p>
                        <p>error: {{ props.error }}</p>
                        <p>items: {{ props.items }}</p>
                        <p>totalItems: {{ props.totalItems }}</p>
                        <p>criteria: {{ props.criteria }}</p>
                        <p>page: {{ props.page }}</p>
                        <p>page size: {{ props.pageSize }}</p>
                        <hr />
                        <button @click="props.setPage(2)">set page to 2</button>
                        <button @click="props.setPageSize(20)">set page size to 20</button>
                    </div>
                </renderless-paged-data-provider>
                <button @click="load">Reload</button>
        </div>`,
        computed: {
            provider: () => provider
        },
        methods: {
            load() {
                this.$refs.loader.load();
            }
        }
    }))
;
