import Vue from 'vue';
import {storiesOf} from '@storybook/vue';


Object.entries(require('../')).forEach(([name, component]) => Vue.component(name, component));

const items = [
    {id: 1, name: 'Minsk'},
    {id: 2, name: 'Warsaw'},
    {id: 3, name: 'Prague'},
    {id: 4, name: 'Tokyo'},
    {id: 5, name: 'Zurich'},
];

function provider() {
    return new Promise(resolve => {
        setTimeout(() => resolve(items), 2000);
    });
}

storiesOf('DataProvider/DataProvider', module)
    .add('default', () => ({
        template: `<div>
                <data-provider autoload :provider="provider" ref="loader">
                    <div slot-scope="{items, isLoading, error, isFirstLoad}">
                        <p>isLoading: {{ isLoading }}</p>
                        <p>isFirstLoad: {{ isFirstLoad }}</p>
                        <p>error: {{ error }}</p>
                        <p>items: {{ items }}</p>
                    </div>
                </data-provider>
                <button @click="load">Reload</button>
        </div>`,
        computed: {
            provider: () => provider
        },
        methods: {
            load() {
                this.$refs.loader.load();
            }
        }
    }))
;
