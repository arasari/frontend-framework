import {storiesOf} from '@storybook/vue';

import Vue from 'vue';
import image from '../empty-state.png';

Object.entries(require('../../buttons')).forEach(([name, component]) => Vue.component(name, component));
Object.entries(require('..')).forEach(([name, component]) => Vue.component(name, component));


storiesOf('Utils/EmptyState', module)
    .add('minimal', () => ({
        template: `<empty-state>No content</empty-state>`,
    }))
    .add('with heading', () => ({
        template: `<empty-state heading="No content">Please, check this page later.</empty-state>`,
    }))
    .add('with image', () => ({
        template: `<empty-state image="${image}" heading="No content">Please, check this page later.</empty-state>`,
    }))
    .add('with actions', () => ({
        template: `<empty-state image="${image}" heading="No content">
            Please, check this page later.
            
            <template slot="action">
                <btn color="primary" icon="fa fa-plus">Create</btn>
            </template>
        </empty-state>`,
    }))
;
