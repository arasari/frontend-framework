import {storiesOf} from '@storybook/vue';
import {action} from '@storybook/addon-actions';

import Vue from 'vue';

Object.entries(require('..')).forEach(([name, component]) => Vue.component(name, component));


storiesOf('Utils/CopyText', module)
    .add('default', () => ({
        template: `<copy-text @copy="onCopy">Click on me and then paste</copy-text>`,
        methods: {
            onCopy: action('copied')
        }
    }))
;
