import {formatNumber} from 'libphonenumber-js';

export function phone(value, format = 'International') {
    return formatNumber(value, format);
}
