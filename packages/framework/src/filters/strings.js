export function lower(value) {
    return value.toLowerCase();
}

export function capitalize(value) {
    return value.charAt(0).toUpperCase() + value.substring(1);
}
