export function userPhoto(user) {
    if (user.photo_url) {
        return user.photo_url;
    }
    return [
        '//www.gravatar.com/avatar',
        user.email_hash,
        '?s=200', '&d=identicon'
    ].join('/');
}
