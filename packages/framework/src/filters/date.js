import moment from 'moment';

export function timeAgo(value) {
    return moment(value).local().fromNow();
}

export function datetime(value, format = 'DD.MM.YYYY HH:mm') {
    return moment(value).local().format(format);
}

export function date(value, format = 'DD.MM.YYYY') {
    return moment(value).local().format(format);
}
