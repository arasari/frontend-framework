export default {
    props: {
        noTrans: Boolean,
    },
    computed: {
        labelProp() {
            return 'label';
        },
        translatedLabel() {
            if (this.noTrans) {
                return this[this.labelProp];
            }
            return this.$t(this[this.labelProp]);
        }
    }
};
