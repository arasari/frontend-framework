export default {
    computed: {
        user() {
            return this.$store.getters['auth/user'];
        },
    },
    methods: {
        cloneUser(fields) {
            const user = {};
            const clone = this.removeReactivity(this.user);
            fields.forEach(f => {
                user[f] = clone[f];
            });
            return user;
        }
    }
};
