export default {
    methods: {
        removeReactivity(data) {
            return JSON.parse(JSON.stringify(data));
        }
    }
};
