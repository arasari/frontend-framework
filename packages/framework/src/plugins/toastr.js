import toastr from 'toastr';
import 'toastr/toastr.scss';

const DefaultOptions = {
    positionClass: 'toast-bottom-right',
};

export default {
    install(Vue, options) {
        toastr.options = Object.assign({}, DefaultOptions, options);

        Vue.prototype.$toast = toastr;
    }
};
