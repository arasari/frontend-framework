export default {
    install(Vue) {
        Vue.prototype.$clipboard = {
            copy(text) {
                const selection = window.getSelection();
                const range = document.createRange();
                const div = document.createElement('div');
                div.innerText = text;
                document.body.appendChild(div);

                range.selectNodeContents(div);
                selection.removeAllRanges();
                selection.addRange(range);

                try {
                    document.execCommand('copy');
                    selection.removeAllRanges();
                } catch (e) {
                    console.error(e);
                } finally {
                    div.parentNode.removeChild(div);
                }
            }
        };
    }
};
