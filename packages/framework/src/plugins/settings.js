export default {
    install(Vue) {
        Vue.mixin({
            beforeCreate() {
                this.$settings = this.$root.$options.settings;
            }
        });
    }
};
