import Plugin from './plugin';
import {default as middleware} from './router/middleware';

export * from './store/support';
export * from './router/support';
export * from './lang/support';
export {default as FieldMixin} from './components/forms/fields/field';

export {middleware};
export default Plugin;
