export function alerts(state) {
    return state.alerts.filter(alert => !state.dismissedAlerts.includes(alert.id));
}

export function contentBlocks(state) {
    return state.contentBlocks;
}

export function menuItems(state) {
    return state.menuItems;
}
