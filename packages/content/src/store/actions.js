import axios from 'axios';
import storage from 'localforage';

export async function getAlerts({commit}) {
    const alerts = await axios.get('/content/alerts/').then(r => r.data);
    commit('setAlerts', alerts);
    return alerts;
}

export async function dismissAlert({commit}, id) {
    commit('dismissAlert', id);
    const cached = (await storage.getItem('dismissedAlerts')) || [];
    cached.push(id);
    storage.setItem('dismissedAlerts', cached);
}

export async function getDismissedAlerts({commit}) {
    const ids = (await storage.getItem('dismissedAlerts')) || [];
    ids.forEach(id => commit('dismissAlert', id));
}

export async function getContentBlocks({commit}) {
    const cached = await storage.getItem('contentBlocks');
    if (cached) {
        commit('setContentBlocks', cached);
    }
    const blocks = await axios.get('/content/content-blocks/').then(r => r.data);
    commit('setContentBlocks', blocks);
    storage.setItem('contentBlocks', blocks);
    return blocks;
}

export async function getMenuItems({commit}) {
    const cached = await storage.getItem('menuItems');
    if (cached) {
        commit('setMenuItems', cached);
    }

    const items = await axios.get('/content/menu-items/').then(r => r.data);
    commit('setMenuItems', items);
    storage.setItem('menuItems', items);
    return items;
}
