export function setAlerts(state, alerts) {
    state.alerts = alerts;
}

export function dismissAlert(state, id) {
    state.dismissedAlerts.push(id);
}

export function setContentBlocks(state, contentBlocks) {
    state.contentBlocks = contentBlocks;
}

export function setMenuItems(state, items) {
    state.menuItems = items;
}
