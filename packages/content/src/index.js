import Plugin from './plugin';

export {default as store} from './store';

export default Plugin;
