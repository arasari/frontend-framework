import storeModule from './store';
import * as components from './components';

export default {
    install(Vue, {store}) {
        store.registerModule('content', storeModule);
        Object
            .entries(components)
            .forEach(([name, component]) => Vue.component(name, component));
    }
};
