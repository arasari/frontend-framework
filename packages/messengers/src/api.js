import axios from 'axios';

export default {
    async disconnect(messenger) {
        return axios.delete(`/messengers/disconnect/${messenger}/`);
    },

    async check(messenger) {
        return axios.post(`/messengers/check/${messenger}/`);
    },
    async getStatus() {
        return axios.get(`/messengers/status/`).then(r => r.data);
    }
};
