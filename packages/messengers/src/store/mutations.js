export function setStatus(state, status) {
    state.telegramConnected = status.telegram;
    state.viberConnected = status.viber;
}
