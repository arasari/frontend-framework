import api from '../api';
export async function getStatus({ commit }) {
    const status = await api.getStatus();
    commit('setStatus', status);
    return status;
}
