import module from './store';

export default {
    install(Vue, { store }) {
        store.registerModule('messengers', module);
    }
};
