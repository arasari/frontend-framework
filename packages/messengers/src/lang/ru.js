export default {
    title: {
        connect_messengers: 'Чат-боты',
    },
    text: {
        connect_bot_and_send_code: 'Для подключения к боту, отправьте ему сообщение с кодом ниже:',
    },
    toast: {
        test_message_sent: 'Отправлено',
        messengers: {
            code_copied: 'Код скопирован в буфер',
        }
    },
    button: {
        connect_bot: 'Подключить',
        disconnect_bot: 'Отключить',
        send_test_message: 'Прислать тестовое сообщение',
    }
};
