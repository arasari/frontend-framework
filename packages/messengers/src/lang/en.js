export default {
    title: {
        connect_messengers: 'Connect messengers',
    },
    text: {
        connect_bot_and_send_code: 'Send the code below to your preferred messenger to link your account.',
    },
    toast: {
        test_message_sent: 'Message has been sent.',
        messengers: {
            code_copied: 'Copied',
        }
    },
    button: {
        connect_bot: 'Link',
        disconnect_bot: 'Unlink',
        send_test_message: 'Send test message',
    }
};
