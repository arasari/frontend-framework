import Plugin from './plugin';

export {default as AppLayout} from './layout/AppLayout';
export {default as AuthLayout} from './layout/AuthLayout';
export {default as EmptyAppLayout} from './layout/EmptyAppLayout';
export {default as SimpleLayout} from './layout/SimpleLayout';

export * from './components';

export default Plugin;
