export default {
    install() {
        import('jquery').then(jquery => window.jQuery = window.$ = jquery);
        import('bootstrap');
        import('font-awesome/css/font-awesome.css');
        import('feathericon/build/css/feathericon.css');
        import('./assets/css/dashboard.css');
        import('./assets/css/extra.scss');
    }
};
