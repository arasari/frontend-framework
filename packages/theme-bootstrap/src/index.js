export default {
    install() {
        import('jquery').then(jquery => window.jQuery = window.$ = jquery);
        import('bootstrap');
        import('bootstrap/dist/css/bootstrap.css');
        import('font-awesome/css/font-awesome.css');
    }
};
