import axios from 'axios';
import storage from 'localforage';
import Vue from 'vue';

/**
 * This objects handler client's authentication.
 *
 * The flow is the following:
 *
 * 1. Interactive login.
 *  This case happens when user manually authenticates itself via login form.
 *
 *  2. Passive login.
 *  In this case we authenticate user using its persisted token.
 *  We do not validate a token. There is an axios response interceptor which checks response status code
 *  and makes further decisions.
 */
export default {
    /**
     * Inner Vue instance used for event subscribe/unsubscribe.
     */
    _$vm: new Vue(),

    /**
     * API token.
     */
    token: null,

    /**
     * Check if current client is authenticated.
     * @return {boolean}
     */
    get isAuthenticated() {
        return !!this.token;
    },

    /**
     * Interactively login user.
     *
     * @param email
     * @param password
     * @return {Promise<*>}
     */
    async login(email, password) {
        try {
            const {token} = await axios.post('/login/', {email, password}).then(r => r.data);
            this._$vm.$emit('login');
            await this.authenticate(token);
            return {token};
        } catch (e) {
            return Promise.reject(e);
        }
    },

    /**
     * Logout user and clear credentials.
     * @return {Promise<void>}
     */
    async logout() {
        await axios.post('/logout/');
        await this.unauthenticate();
        this._$vm.$emit('unauthenticated');
    },

    /**
     * Authenticate current client.
     * @param token
     * @return {Promise<void>}
     */
    async authenticate(token) {
        await Promise.all([
            storage.setItem('token', token)
        ]);
        this.token = token;
        this._$vm.$emit('authenticated', token);
    },

    /**
     * Unauthenticate client.
     * @return {Promise<void>}
     */
    async unauthenticate() {
        this.token = null;
        await Promise.all([
            storage.removeItem('token')
        ]);
    },

    /**
     * Check client authentication.
     * @return {Promise<void>}
     */
    async verify() {
        const token = await storage.getItem('token');
        if (token) {
            this.token = token;
            await this.authenticate(token);
        }
        this._$vm.$emit('ready');
    },

    /**
     * Add auth events listener.
     * @param event
     * @param cb
     */
    on(event, cb) {
        this._$vm.$on(event, cb);
    },

    async requestPasswordReset(email) {
        return axios.post('/password/forgot/', {email});
    },

    async resetPassword(payload) {
        return axios.post('/password/reset/', payload);
    },

    async resendEmailVerification() {
        return axios.post('/account/verify/resend/');
    },

    async changePassword({password, current_password}) {
        return axios.post('/password/change/', {password, current_password});
    },

};
