import auth from './auth';
import {RequireAuth, RequirePermissions} from './components';
import storeModule from './store';
import {installAuthGuard, installAxiosRequestInterceptor, installAxiosResponseInterceptor} from './support';

const globalComponents = {RequirePermissions, RequireAuth};

export default {
    install(Vue, {store, router, axios}) {
        // register global components
        Object.entries(globalComponents).forEach(([name, component]) => Vue.component(name, component));

        // attach Vuex module
        store.registerModule('auth', storeModule);

        // this helper adds token header to each request
        installAxiosRequestInterceptor({store, router, axios});

        // this helper checks response status code and does user logout if 401 received
        installAxiosResponseInterceptor({store, router, axios});

        // adds global router hook which prevents unauthorized access to protected app pages
        installAuthGuard(router);

        // proxy: notify store that authentication happened
        auth.on('authenticated', token => {
            store.dispatch('auth/authenticate', token);
            store.dispatch('auth/getPermissions');
        });

        // proxy: notify store that user session has ended
        auth.on('unauthenticated', () => {
            store.dispatch('auth/unauthenticate');
            store.commit('auth/setPermissions', ['unauthenticated']);
        });

        import('axios').then(axios => {
            installAxiosRequestInterceptor({store, router, axios});
            installAxiosResponseInterceptor({store, router, axios});
        });
    }
};
