import * as actions from './actions';
import * as mutations from './mutations';
import * as getters from './getters';

const state = {
    ready: false,
    token: null,
    user: {
        id: 0, full_name: '', email: '',
    },
    permissions: ['unauthenticated']
};

export default {
    namespaced: true, state,
    actions, mutations, getters,
};
