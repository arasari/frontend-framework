import axios from 'axios';
import auth from '../auth';

/**
 * Interactive login.
 * Proxies login request to auth service.
 * This service emits "authenticated" event.
 * The listener of this event will handle the received token.
 *
 * @param store
 * @param email
 * @param password
 * @return {Promise<*>}
 */
export async function login(store, {email, password}) {
    try {
        return await auth.login(email, password);
    } catch (e) {
        return Promise.reject(e);
    }
}

/**
 * Authenticate user (non-interactive login).
 * This action ensures that user's identity has been confirmed.
 *
 * @param commit
 * @param token
 * @param user
 * @return {Promise<void>}
 */
export async function authenticate({commit}, token) {
    commit('authenticate', token);
}

/**
 * Logout user.
 * @return {Promise<void>}
 */
export async function logout() {
    return await auth.logout();
}

/**
 * Clean up API tokens.
 * @param commit
 * @return {Promise<void>}
 */
export async function unauthenticate({commit}) {
    commit('logout');
}

/**
 * Fetch information about current user.
 * @param commit
 * @return {Promise<void>}
 */
export async function getMe({commit}) {
    const user = await axios.get('/auth/me/').then(r => r.data);
    commit('setUser', user);
}

/**
 * Fetch user's permissions from API.
 * @param commit
 * @returns {Promise<void>}
 */
export async function getPermissions({commit}) {
    const permissions = await axios.get('/auth/permissions/').then(r => r.data);
    commit('setPermissions', permissions);
}

/**
 * Update user profile.
 * @param commit
 * @param data
 * @returns {Promise<void>}
 */
export async function updateProfile({commit}, data) {
    const profile = await axios.patch('/auth/me/', data).then(r => r.data);
    commit('updateProfile', profile);
}
