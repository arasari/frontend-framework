/**
 * Returns current authentication status.
 * @param store
 * @return {boolean}
 */
export function authenticated(store) {
    return !!store.token;
}

/**
 * Returns current user info.
 * @param store
 * @return {store.user|{}}
 */
export function user(store) {
    return store.user;
}

/**
 * Check if user has confirmed its email address.
 * @param store
 * @returns {*}
 */
export function emailVerified(store) {
    return store.user.email_verified;
}


/**
 * Check if user has confirmed its email address.
 * @param store
 * @returns {*}
 */
export function phoneVerified(store) {
    return store.user.phone_verified;
}
