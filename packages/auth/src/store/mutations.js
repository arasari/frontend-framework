import Vue from 'vue';

/**
 * Persist token and user in storage.
 * @param state
 * @param token
 */
export function authenticate(state, token) {
    state.token = token;
}

/**
 * Kill user session.
 * @param state
 */
export function logout(state) {
    state.token = null;
    state.user = {};
}

/**
 * Set current user.
 * @param state
 * @param user
 */
export function setUser(state, user) {
    state.user = user;
}

/**
 * Set user permissions.
 * @param state
 * @param permissions
 */
export function setPermissions(state, permissions) {
    state.permissions = permissions;
}

/**
 * Update user info.
 * @param state
 * @param profile
 */
export function updateProfile(state, profile) {
    const newUser = Object.assign({}, state.user, profile);
    Vue.set(state, 'user', newUser);
}
