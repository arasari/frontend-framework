/**
 * Convert value to array if it is a string.
 * @param {String} value
 * @returns {Array<String>}
 */
function toArray(value) {
    if (value.constructor === String) {
        value = [value];
    }
    return value;
}

function check(perm, all) {
    if (perm[0] === '!' || perm[0] === '~') {
        return all.indexOf(perm.substr(1)) === -1;
    }
    return all.indexOf(perm) !== -1;
}

/**
 * Test if all requiredPermissions are in userPermissions.
 *
 * @param {Array<String>} requiredPermissions
 * @param {Array<String>} userPermissions
 */
export function canAll(requiredPermissions, userPermissions) {
    return toArray(requiredPermissions).every(perm => {
        return check(perm, userPermissions);
    });
}

/**
 * Test if any of requiredPermissions is in userPermissions.
 *
 * @param {Array<String>} requiredPermissions
 * @param {Array<String>} userPermissions
 */
export function canAny(requiredPermissions, userPermissions) {
    return toArray(requiredPermissions).some(perm => {
        return check(perm, userPermissions);
    });
}
