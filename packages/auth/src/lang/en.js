export default {
    link: {
        forgot_password: 'I forgot password',
        register_now: 'Sign up',
        question_no_account: 'Don\'t have account yet?',
        question_have_account: 'Already have account?',
        sign_in: 'Sign in',
        agree_terms_and_conditions: 'terms and policy',
    },
    menu: {
        sign_out: 'Sign out',
        sign_in: 'Sign in',
        registration: 'Registration'
    },
    label: {
        email: 'Email',
        name: 'Name',
        full_name: 'Name',
        avatar: 'Photo',
        current_password: 'Current password',
        new_password: 'New password',
        password_confirmation: 'Confirm password',
        description: 'Description',
        phone: 'Phone',
        password: 'Password',
        agree_with_terms: 'Agree with ',
        first_name: 'First name',
        last_name: 'Last name',
    },
    page_title: {
        sign_in: 'Sign in',
        password_reset: 'Password reset',
        set_new_password: 'Change password',
        register: 'Registration',
    },
    placeholder: {
        enter_email: 'Enter E-mail address',
        enter_password: 'Enter new password',
        confirm_password: 'Enter new password',
    },
    title: {
        login: 'Sign in',
        sign_in: 'Sign in',
        password_reset: 'Password reset',
        set_new_password: 'Set new password',
        register: 'Create new account',
    },
    text: {
        email_verification_required: 'Verify your email to get the most out of the app.',
        reset_password_text: 'Enter your email address and we will email your with further instructions',
        didnt_get_verification_email: "Didn't receive the email? Check the Spam folder, it may have been caught by a filter. If you still don't see it, you can"
    },
    toast: {
        password_changed: 'Password has been changed',
        password_reset_link_sent: 'Password reset link has been sent.',
        password_reset: 'Password has been reset.',
        email_verification_resent: 'A verification email has been sent.',
    },
    button: {
        login: 'Sign in',
        save: 'Save',
        change_password: 'Change password',
        reset_password: 'Send instructions',
        create_account: 'Create new account',
        resend_email_verification: 'resend the confirmation email.',
        set_new_password: 'Set new password',
    },
};
