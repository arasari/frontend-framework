export default {
    link: {
        forgot_password: 'Забыли пароль?',
        register_now: 'Создать аккаунт',
        question_no_account: 'У вас нет аккаунта?',
        question_have_account: 'Уже есть аккаунт?',
        sign_in: 'Войти',
        agree_terms_and_conditions: 'правила пользования сайтом',
    },
    menu: {
        sign_out: 'Выйти',
        sign_in: 'Вход',
        registration: 'Регистрация'
    },
    label: {
        email: 'Email',
        name: 'Имя',
        full_name: 'Полное имя',
        avatar: 'Фото',
        current_password: 'Текущий пароль',
        new_password: 'Новый пароль',
        password_confirmation: 'Повторите пароль',
        description: 'Описание',
        phone: 'Телефон',
        password: 'Пароль',
        agree_with_terms: 'Принимаю ',
        first_name: 'Имя',
        last_name: 'Фамилия',
    },
    page_title: {
        sign_in: 'Вход',
        password_reset: 'Смена пароля',
        set_new_password: 'Сменить пароль',
        register: 'Регистрация',
    },
    placeholder: {
        enter_email: 'Введите адрес E-mail',
        enter_password: 'Введите новый пароль',
        confirm_password: 'Повторите новый пароль',
    },
    title: {
        login: 'Войти',
        sign_in: 'Войти',
        password_reset: 'Сбросить пароль',
        set_new_password: 'Задать новый пароль',
        register: 'Создать новый аккаунт',
    },
    text: {
        email_verification_required: 'Подтвердите ваш e-mail, что бы разблокировать все возможности сайта.',
        reset_password_text: 'Введите ваш e-mail и мы вышлем вам дальнейшие инструкции',
        didnt_get_verification_email: 'Не получили письмо?  Проверьте в папке "Спам". Либо вы можете '
    },
    toast: {
        password_changed: 'Пароль был изменен!',
        password_reset_link_sent: 'Инструкции для сброса пароля были отправлены на указанный адрес почты.',
        password_reset: 'Пароль был изменен.',
        email_verification_resent: 'Письмо для подтверждения аккаунта было отправлено.',
    },
    button: {
        login: 'Войти',
        save: 'Сохранить',
        change_password: 'Сменить пароль',
        reset_password: 'Выслать инструкции',
        create_account: 'Создать аккаунт',
        resend_email_verification: 'переслать письмо еще раз.',
        set_new_password: 'Задать новый пароль',
    },
};
