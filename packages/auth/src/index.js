import auth from './auth';
import store from './store';
import routes from './routes';
import {installAxiosResponseInterceptor, installAxiosRequestInterceptor} from './support';
import Plugin from './plugin';

export * from './components';

export {auth, store, routes, installAxiosResponseInterceptor, installAxiosRequestInterceptor};
export default Plugin;
