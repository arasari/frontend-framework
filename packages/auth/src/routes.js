export default [
    {
        path: 'login',
        name: 'login',
        component: () => import('./pages/Login'),
    },
    {
        path: 'password/forgot',
        name: 'forgot-password',
        component: () => import('./pages/ForgotPassword'),
    },
    {
        path: 'password/reset/:token',
        name: 'change-password',
        component: () => import('./pages/ResetPassword'),
        props: true,
    }
];
