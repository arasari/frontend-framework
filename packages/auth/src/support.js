import auth from './auth';

let isCheckingForAuth = false;

/**
 * Install per-request hook to axios instance.
 * This hook adds "Authorization: Bearer <token>" header to each HTTP request.
 *
 * Note, the hook is added only if user is authenticated and token is authenticated.
 */
export function installAxiosRequestInterceptor({axios}) {
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    axios.interceptors.request.use(config => {
        if (auth.token && !isCheckingForAuth) {
            config.headers.Authorization = `Bearer ${auth.token}`;
        }
        return config;
    });
}

/**
 * Listens to each HTTP response and trigger logout if any of them fails with status = 401 (Unauthorized).
 */
export function installAxiosResponseInterceptor({axios}) {
    axios.interceptors.response.use(r => r, async(error) => {
        if (error.response.status === 401) {
            if (isCheckingForAuth) {
                return Promise.reject(error);
            }

            try {
                isCheckingForAuth = true;
                await auth.logout();
            } finally {
                isCheckingForAuth = false;
            }
            return Promise.reject(error);
        }
        return Promise.reject(error);
    });
}

/**
 * Adds a global navigation hook to Vue router.
 * This hooks looks for "requiresAuth" in route meta properties.
 * If found it will check if user is authenticated. If not, will redirect to login page.
 *
 * @param router
 * @param loginRoute
 */
export function installAuthGuard(router, loginRoute = 'login') {
    router.beforeEach((to, from, next) => {
        if (to.matched.some(record => record.meta.requiresAuth)) {
            if (!auth.isAuthenticated) {
                next({
                    name: loginRoute,
                    query: {redirect: to.fullPath}
                });
            } else {
                next();
            }
        } else {
            next();
        }
    });
}
