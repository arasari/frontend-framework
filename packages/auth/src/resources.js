import axios from 'axios';

export async function resendEmailCode() {
    return axios.post('/security/email/resend/').then(r => r.data);
}
