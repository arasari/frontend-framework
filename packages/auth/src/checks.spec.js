import {canAll, canAny} from './utils';

describe('permissions.js', () => {
    it('canAll should accept string as required permission', () => {
        expect(canAll('can_edit', ['can_edit', 'can_delete'])).toBeTruthy;
    });

    it('canAny should accept string as required permission', () => {
        expect(canAny('can_edit', ['can_edit', 'can_delete'])).toBeTruthy;
    });

    it('canAll should allow access if permissions match', () => {
        expect(canAll(['can_edit'], ['can_edit', 'can_delete'])).toBeTruthy;
    });

    it('canAll should disallow access if permissions do not match', () => {
        expect(canAll(['can_edit', 'can_update'], ['can_edit', 'can_delete'])).toBeFalsy;
    });

    // allow all users who has not can_edit
    it('canAll should allow access if set has valid negative permission', () => {
        expect(canAll(['~can_edit', 'can_delete'], ['can_delete'])).toBeTruthy;
    });

    // forbid all users who has can_edit
    it('canAll should disallow access if set does not match negative permission', () => {
        expect(canAll(['~can_edit', 'can_delete'], ['can_edit', 'can_delete'])).toBeFalsy;
    });

    // allow all users who has not can_edit
    it('canAny should allow access if set has valid negative permission', () => {
        expect(canAny(['~can_edit', 'can_delete'], ['can_delete'])).toBeTruthy;
    });

    it('canAny should disallow access if set does not match negative permission', () => {
        expect(canAny(['~can_edit', 'can_delete'], ['can_edit'])).toBeFalsy;
    });

    it('canAny should allow access if permissions match', () => {
        expect(canAny(['can_edit', 'can_update'], ['can_edit', 'can_delete'])).toBeTruthy;
    });

    it('canAny should disallow access if permissions do not match', () => {
        expect(canAny(['can_create', 'can_view'], ['can_edit', 'can_delete'])).toBeFalsy;
    });
});
