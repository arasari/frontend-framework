import axios from 'axios';

export async function getTopics() {
    return await axios.get('/faq/').then(r => r.data);
}

export async function getTopic(slug) {
    return await axios.get(`/faq/${slug}/`).then(r => r.data);
}
