import {getTopic, getTopics} from './api';

export default [
    {
        name: 'faq',
        path: 'faq',
        component: () => import('./pages/TopicList.vue'),
    },
    {
        name: 'faq/topic',
        path: 'faq/:slug',
        component: () => import('./pages/ViewTopic.vue'),
        props: true,
    },
];
