module.exports = {
    root: true,
    env: {
        node: true,
        jest: true,
    },
    'extends': [
        'plugin:vue/essential',
        'eslint:recommended'
    ],
    rules: {
        'no-console': 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'generator-star-spacing': 0,
        'semi': [2, 'always'],
        'space-before-function-paren': [2, 'never'],
        'object-property-newline': 0,
    },
    parserOptions: {
        parser: 'babel-eslint'
    }
};
