import {withKnobs} from '@storybook/addon-knobs/src/index';
import {setOptions} from '@storybook/addon-options/src/preview';
import {configure} from '@storybook/vue';
import {addDecorator} from '@storybook/vue/dist/client/preview/index';
import Vue from 'vue';

// import ThemeBootstrap from '../packages/theme-bootstrap';
import ThemeTabler from '../packages/theme-tabler';
import ArasariFramework from '../packages/framework';

import CodeViewer from './components/CodeViewer';
import ExampleDemo from './components/ExampleDemo';
import Markdown from './components/Markdown';

// Vue.use(ThemeBootstrap);
Vue.use(ArasariFramework);
Vue.use(ThemeTabler);

Vue.component('Markdown', Markdown);
Vue.component('ExampleDemo', ExampleDemo);
Vue.component('CodeViewer', CodeViewer);

Vue.mixin({
    filters: {
        trans: msg => msg
    },
    methods: {
        __: msg => msg
    }
});

const withCodePreview = (getStory, options) => {
    const template = getStory().template;
    const description = getStory().description;
    return {
        ...getStory(),
        template: `
            <div>
                <h2>${options.kind}</h2>
                <p>${options.story}</p>
                <div v-if="description">
                <markdown>{{ description }}</markdown>
                </div>
                <example-demo>${template}</example-demo>
                <br />
                <hr />
                <code-viewer :code="template"></code-viewer>
            </div>
        </div>`,
        computed: {
            ...(getStory().computed || {}),
            description() {
                return description;
            },
            template() {
                return template;
            },
        }
    };
};

addDecorator(withCodePreview);
addDecorator(withKnobs);

setOptions({
    name: 'Arasari Components',
    goFullScreen: false,
    showStoriesPanel: true,
    showAddonPanel: true,
    showSearchBox: false,
    addonPanelInRight: true
});

const req = require.context('../packages/', true, /\.stories\.js$/);

function loadStories() {
    req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
